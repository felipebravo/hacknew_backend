# Hacker News

Node + Express + Mongo
# Description
_consume api hacker new and save data in mongodb database END-POINT_
* [https://hn.algolia.com/api/v1/search_by_date?query=nodejs] (https://hn.algolia.com/api/v1/search_by_date?query=nodejs)

### Instructions

### Install Package 🔧
```npm install```

### Run 📦
Porduction: ```npm start```
Developer: ```npm run dev```

### Docker 🛠️
docker-compose up

### Note

If it changes, please change the ip of the db / connection.js file to localhost.
If you are going to docker, please enter the local IP of your computer