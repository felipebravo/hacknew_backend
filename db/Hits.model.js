// Hits.model.js
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    story_title: String,
    author: String,
    story_url: String,
    created_at: Date,
    objectID: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: function(value) {
                const self = this;
                const errorMsg = 'objectID exist!';
                return new Promise((resolve, reject) => {
                    self.constructor.findOne({ objectID: value })
                        .then(model => model._id ? reject(new Error(errorMsg)) : resolve(true)) // if _id found then objectID exist 
                        .catch(err => resolve(true)) // make sure to check for db errors here
                });
            }
        }
    },
    deleted:Boolean,
});
const Hits = mongoose.model("Hits", userSchema);
module.exports = Hits;