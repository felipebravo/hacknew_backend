config = require('./configs/config');//Pass generate token
const axios = require('axios');
const cron = require('node-schedule');
const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs&hitsPerPage=20';

const connectDb = require("./db/connection");
const Hits = require("./db/Hits.model");

module.exports = {
    auth: auth,
    getnews: getnews,
    updatenew: updatenew,
    scheduleJob: scheduleJob,
    startup: startup
};

//conection MONGO
const cn = connectDb().then(() => {
    console.log("MongoDb connected");
}); 

//Job for hour
function scheduleJob() {
    cron.scheduleJob('0 */1 * * *', function () {
        startup();
    });
}

//Generate Token
function auth(req, res, next) {
    const payload = { check: true };
    const token = jwt.sign(payload, config.llave, {
        expiresIn: 525600
    });
    res.json({
        message: 'Autenticación correcta',
        token: token
    });
}

function startup() {
    rq_call_api().then(hits => {
        insertnewsDB(hits);
    })
}

//Call API HN
function rq_call_api() {
    return new Promise(function (resolve) {
        axios.get(url)
            .then(json => {
                console.log('Hits loaded', json.data.hits.length)
                //return promise
                resolve(json.data.hits);
            }).catch(function (error) {
                console.error('Error', error);
            })
    })
}

//insert return values ​​from api
function insertnewsDB(hits) {
    try {
        cn.then(() => {
            console.log("MongoDb connected");
            hits.forEach(function (newsItem) {
                var hits = new Hits({
                    story_title: newsItem.story_title,
                    author: newsItem.author,
                    story_url: newsItem.story_url,
                    created_at: newsItem.created_at,
                    objectID: newsItem.created_at,
                    deleted: false,
                });

                hits.save(function (err) {
                    if (err) {
                        console.log(err.message);
                    } else {
                        console.log('Hits successfully saved.');
                    }
                });
            })
        });
    } catch (e) {
        //print(e);
    }
}

//get hits from DB Mongo
function getnews(req, res, next) {
    const token = req.body.token;
    jwt.verify(token, config.llave, (err, decoded) => {
        if (err) {
            return res.json({ mensaje: 'Invalid token' });
        } else {
            cn.then(() => {
                Hits.find({}).then((items) =>
                    res.send(items)
                );
            })
        }
    });
};

//Update record "Deleted" Mongo
function updatenew(req, res, next) {
    const id = req.body.id;
    const token = req.body.token;
    jwt.verify(token, config.llave, (err, decoded) => {
        if (err) {
            return res.json({ mensaje: 'Invalid token' });
        } else {
            const filter = { objectID: id };
            const update = { deleted: true };
            cn.then(() => {
                Hits.findOneAndUpdate(filter, update, {
                    new: true
                }).then((data) =>
                    res.send(data)
                );
            })
        }
    });
};







