var express = require('express');
var router = express.Router();

var db = require('../queries');

router.post('/api/auth', db.auth);
router.post('/api/getnews', db.getnews);
router.post('/api/updatenew', db.updatenew);


/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
  });



module.exports = router;
